package mk.ukim.finki.emt.vehiclecatalogue.domain.valueobjects;

public enum MeasurementUnitOfPower {
    HP, KW
}
