package mk.ukim.finki.emt.vehiclecatalogue.service.forms;

import lombok.Data;
@Data
public class VehicleForm {

    private String brand;

    private String model;

    private String year;

    private String registration;

    private double power;

    private String fuelType;
}
