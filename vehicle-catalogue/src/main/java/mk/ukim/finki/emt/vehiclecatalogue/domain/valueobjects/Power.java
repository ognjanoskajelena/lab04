package mk.ukim.finki.emt.vehiclecatalogue.domain.valueobjects;

import lombok.Getter;
import mk.ukim.finki.emt.sharedkernel.domain.base.ValueObject;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Objects;

/**
 * Репрзентира објект кој содржи моќност како вредност.
 * Имплементира можни операции со истата, т.е. обезбедува конверзија во различни мерни единици.
 */
@Embeddable
@Getter
public class Power implements ValueObject {

    @Enumerated(value = EnumType.STRING)
    private final MeasurementUnitOfPower unit;

    private final double power;

    protected Power() {
        unit = null;
        power = 0.0;
    }

    public Power(MeasurementUnitOfPower unit, double amount) {
        this.unit = unit;
        this.power = amount;
    }

    public static Power valueOf(MeasurementUnitOfPower unit, double amount) {
        return new Power(unit, amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Power)) return false;
        Power power1 = (Power) o;
        return Double.compare(power1.power, power) == 0 && unit == power1.unit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(unit, power);
    }

    public double toHorsePower() {
        if (unit.equals(MeasurementUnitOfPower.HP))
            return power;
        else return power * 1.36;
    }

    public double toKW() {
        if (unit.equals(MeasurementUnitOfPower.KW))
            return power;
        else return power / 1.36;
    }
}
