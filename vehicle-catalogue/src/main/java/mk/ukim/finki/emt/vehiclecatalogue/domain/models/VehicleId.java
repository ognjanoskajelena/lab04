package mk.ukim.finki.emt.vehiclecatalogue.domain.models;

import mk.ukim.finki.emt.sharedkernel.domain.base.DomainObjectId;
import org.springframework.lang.NonNull;

public class VehicleId extends DomainObjectId {

    private VehicleId() {
        super(VehicleId.randomId(VehicleId.class).getId());
    }

    public VehicleId(@NonNull String uuid) {
        super(uuid);
    }

    public static VehicleId of(String uuid) {
        return new VehicleId(uuid);
    }
}
