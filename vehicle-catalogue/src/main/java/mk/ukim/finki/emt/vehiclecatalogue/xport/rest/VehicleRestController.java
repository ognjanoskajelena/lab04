package mk.ukim.finki.emt.vehiclecatalogue.xport.rest;

import lombok.AllArgsConstructor;
import mk.ukim.finki.emt.vehiclecatalogue.domain.models.Vehicle;
import mk.ukim.finki.emt.vehiclecatalogue.domain.models.VehicleId;
import mk.ukim.finki.emt.vehiclecatalogue.service.VehicleService;
import mk.ukim.finki.emt.vehiclecatalogue.service.forms.VehicleForm;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest controller за vehicle-catalogue агрегатот комуницира со "надворешниот свет".
 */
@RestController
@RequestMapping("/api/vehicle")
@CrossOrigin(origins = "http://localhost:3000")
@AllArgsConstructor
public class VehicleRestController {

    private final VehicleService vehicleService;

    @GetMapping
    public List<Vehicle> findAll() {
        return this.vehicleService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Vehicle> findById(@PathVariable VehicleId id) {
        return this.vehicleService.findById(id)
                .map(v -> ResponseEntity.ok().body(v))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @PostMapping("/add")
    public ResponseEntity<Vehicle> save(@RequestBody VehicleForm vehicleForm) {
        return this.vehicleService.createVehicle(vehicleForm)
                .map(v -> ResponseEntity.ok().body(v))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }
}
