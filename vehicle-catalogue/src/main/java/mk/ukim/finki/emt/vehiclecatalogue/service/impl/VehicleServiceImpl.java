package mk.ukim.finki.emt.vehiclecatalogue.service.impl;

import lombok.AllArgsConstructor;
import mk.ukim.finki.emt.vehiclecatalogue.domain.models.Vehicle;
import mk.ukim.finki.emt.vehiclecatalogue.domain.models.VehicleId;
import mk.ukim.finki.emt.vehiclecatalogue.domain.repository.VehicleRepository;
import mk.ukim.finki.emt.vehiclecatalogue.domain.valueobjects.MeasurementUnitOfPower;
import mk.ukim.finki.emt.vehiclecatalogue.domain.valueobjects.Power;
import mk.ukim.finki.emt.vehiclecatalogue.service.VehicleService;
import mk.ukim.finki.emt.vehiclecatalogue.service.forms.VehicleForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Сервисот за vehicle-catalogue агрегатот ја оркестрира бизнис логиката инкорпорирана во доменот
 * и дополнително користи помошни технологии.
 */
@Service
@Transactional
@AllArgsConstructor
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;

    @Override
    public Optional<Vehicle> findById(VehicleId id) {
        return this.vehicleRepository.findById(id);
    }

    @Override
    public Optional<Vehicle> createVehicle(VehicleForm form) {
        Vehicle vehicle = Vehicle.build(form.getBrand(), form.getModel(), form.getYear(), form.getRegistration(),
                Power.valueOf(MeasurementUnitOfPower.KW, form.getPower()), form.getFuelType());
        this.vehicleRepository.save(vehicle);
        return Optional.of(vehicle);
    }

    @Override
    public List<Vehicle> findAll() {
        return this.vehicleRepository.findAll();
    }
}
