package mk.ukim.finki.emt.vehiclecatalogue.domain.repository;

import mk.ukim.finki.emt.vehiclecatalogue.domain.models.Vehicle;
import mk.ukim.finki.emt.vehiclecatalogue.domain.models.VehicleId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleRepository extends JpaRepository<Vehicle, VehicleId> {
}
