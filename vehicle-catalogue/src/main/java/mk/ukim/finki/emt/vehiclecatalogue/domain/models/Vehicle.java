package mk.ukim.finki.emt.vehiclecatalogue.domain.models;

import lombok.Getter;
import mk.ukim.finki.emt.sharedkernel.domain.base.AbstractEntity;
import mk.ukim.finki.emt.vehiclecatalogue.domain.valueobjects.Power;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Ентитетот Vehicle е единствениот во својот агрегат, па соодветно претставува Aggregate Root.
 */
@Entity
@Getter
@Table(name = "vehicle")
public class Vehicle extends AbstractEntity<VehicleId> {

    private String brand;

    private String model;

    private String year;

    private String registration;

    private Power power;

    private String fuelType;

    private Vehicle() {
        super(VehicleId.randomId(VehicleId.class));
    }

    public static Vehicle build(String brand, String model, String year,
                                String registration, Power power, String fuelType) {
        Vehicle vehicle = new Vehicle();
        vehicle.brand = brand;
        vehicle.model = model;
        vehicle.year = year;
        vehicle.registration = registration;
        vehicle.power = power;
        vehicle.fuelType = fuelType;
        return vehicle;
    }
}
