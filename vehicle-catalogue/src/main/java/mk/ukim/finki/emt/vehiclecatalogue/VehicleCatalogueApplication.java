package mk.ukim.finki.emt.vehiclecatalogue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleCatalogueApplication {

    public static void main(String[] args) {
        SpringApplication.run(VehicleCatalogueApplication.class, args);
    }

}
