package mk.ukim.finki.emt.vehiclecatalogue.config;

import lombok.AllArgsConstructor;
import mk.ukim.finki.emt.vehiclecatalogue.domain.models.Vehicle;
import mk.ukim.finki.emt.vehiclecatalogue.domain.repository.VehicleRepository;
import mk.ukim.finki.emt.vehiclecatalogue.domain.valueobjects.MeasurementUnitOfPower;
import mk.ukim.finki.emt.vehiclecatalogue.domain.valueobjects.Power;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Component
@AllArgsConstructor
public class DataInitializer {
    private final VehicleRepository vehicleRepository;

    @PostConstruct
    public void initData() {
        Vehicle v1 = Vehicle.build("Volkswagen", "Golf VII", "2014","GV-0001-PP",
                Power.valueOf(MeasurementUnitOfPower.KW, 81), "Diesel");
        Vehicle v2 = Vehicle.build("Mercedes", "G63", "2020", "GV-0002-AB",
                Power.valueOf(MeasurementUnitOfPower.HP, 585), "Petrol");
        Vehicle v3 = Vehicle.build("BMW", "E36 328I", "1998", "GV-0003-BC",
                Power.valueOf(MeasurementUnitOfPower.HP, 193), "Petrol");
        if (vehicleRepository.findAll().isEmpty()) {
            vehicleRepository.saveAll(Arrays.asList(v1, v2, v3));
        }
    }
}
