package mk.ukim.finki.emt.vehiclecatalogue.service;

import mk.ukim.finki.emt.vehiclecatalogue.domain.models.Vehicle;
import mk.ukim.finki.emt.vehiclecatalogue.domain.models.VehicleId;
import mk.ukim.finki.emt.vehiclecatalogue.service.forms.VehicleForm;

import java.util.List;
import java.util.Optional;

public interface VehicleService {

    Optional<Vehicle> findById(VehicleId id);

    Optional<Vehicle> createVehicle(VehicleForm form);

    List<Vehicle> findAll();
}
