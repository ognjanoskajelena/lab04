package mk.ukim.finki.emt.casemanagement.domain.models;

import lombok.Getter;
import mk.ukim.finki.emt.sharedkernel.domain.base.AbstractEntity;
import mk.ukim.finki.emt.sharedkernel.domain.base.DomainObjectId;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Money;
import org.springframework.lang.NonNull;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * Ентитетот Issue е исто така дел од агрегатот case-management, а сите бизнис операции поврзани со истиот
 * се дел од одговорноста на Aggregate root на овој агрегат (Case ентитетот).
 */
@Entity
@Table(name = "issue")
@Getter
public class Issue extends AbstractEntity<IssueId> {

    private String description;

    @Enumerated(value = EnumType.STRING)
    private IssueState state;

    private Money repairCost;

    private Issue() {
        super(DomainObjectId.randomId(IssueId.class));
    }

    public Issue(String description, @NonNull Money repairCost) {
        super(DomainObjectId.randomId(IssueId.class));
        this.description = description;
        this.state = IssueState.NOT_FIXED;
        this.repairCost = repairCost;
    }

    public void fix() {
        this.state = IssueState.FIXED;
    }
}
