package mk.ukim.finki.emt.casemanagement.domain.models;

import mk.ukim.finki.emt.sharedkernel.domain.base.DomainObjectId;
import org.springframework.lang.NonNull;

public class CaseId extends DomainObjectId {

    private CaseId() {
        super(CaseId.randomId(CaseId.class).getId());
    }

    public CaseId(@NonNull String uuid) {
        super(uuid);
    }

    public static CaseId of(String uuid) {
        return new CaseId(uuid);
    }
}
