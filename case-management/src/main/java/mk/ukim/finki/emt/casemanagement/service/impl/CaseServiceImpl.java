package mk.ukim.finki.emt.casemanagement.service.impl;

import lombok.AllArgsConstructor;
import mk.ukim.finki.emt.casemanagement.domain.exceptions.CaseNotFoundException;
import mk.ukim.finki.emt.casemanagement.domain.exceptions.IssueNotFoundException;
import mk.ukim.finki.emt.casemanagement.domain.models.*;
import mk.ukim.finki.emt.casemanagement.domain.repository.CaseRepository;
import mk.ukim.finki.emt.casemanagement.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.casemanagement.service.CaseService;
import mk.ukim.finki.emt.casemanagement.service.forms.CaseForm;
import mk.ukim.finki.emt.casemanagement.service.forms.IssueFixForm;
import mk.ukim.finki.emt.casemanagement.service.forms.IssueForm;
import mk.ukim.finki.emt.sharedkernel.domain.events.VehicleCaseClosed;
import mk.ukim.finki.emt.sharedkernel.infra.DomainEventPublisher;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Сервисот за case-management агрегатот ја оркестрира бизнис логиката инкорпорирана во доменот
 * и дополнително користи помошни технологии.
 */
@Service
@AllArgsConstructor
public class CaseServiceImpl implements CaseService {

    private final CaseRepository caseRepository;
    private final Validator validator;
    private final DomainEventPublisher domainEventPublisher;

    @Override
    public Optional<Case> openCase(CaseForm caseForm) {
        Objects.requireNonNull(caseForm, "case must not be null");

        var constraintViolations = validator.validate(caseForm);
        if (constraintViolations.size() > 0) {
            throw new ConstraintViolationException("The case form is not valid", constraintViolations);
        }

        var newCase = caseRepository.saveAndFlush(toDomainObject(caseForm));
        return Optional.of(newCase);
    }

    @Override
    public List<Case> findAllByVehicleId(VehicleId id) {
        List<Case> result = new ArrayList<>();
        for (Case c : caseRepository.findAll()) {
            if (c.getVehicleId().getId().equals(id.getId()))
                result.add(c);
        }
        return result;
    }

    @Override
    public Optional<Case> findById(CaseId id) {
        return caseRepository.findById(id);
    }

    @Override
    public List<Issue> findAllIssuesByCase(CaseId id) {
        Optional<Case> optionalCase = findById(id);
        if (optionalCase.isEmpty())
            throw new CaseNotFoundException();
        return new ArrayList<>(optionalCase.get().getIssues());
    }

    @Override
    public Optional<Issue> addIssue(IssueForm issueForm) throws CaseNotFoundException {
        Case aCase = this.caseRepository.findById(issueForm.getCaseId()).orElseThrow(CaseNotFoundException::new);
        Issue issue = aCase.addIssue(issueForm.getDescription(), issueForm.getRepairCost());

        this.caseRepository.saveAndFlush(aCase);
        return Optional.of(issue);
    }

    @Override
    public boolean findIssueByCase(CaseId caseId, IssueId issueId) {
        return findAllIssuesByCase(caseId).stream()
                .anyMatch(issue -> issue.getId().getId().equals(issueId.getId()) &&
                        issue.getState().equals(IssueState.FIXED));
    }

    @Override
    public void fixIssue(IssueFixForm fixForm) throws CaseNotFoundException, IssueNotFoundException {
        Case aCase = this.caseRepository.findById(fixForm.getCaseId()).orElseThrow(CaseNotFoundException::new);
        aCase = aCase.fixIssue(fixForm.getIssueId());

        this.caseRepository.saveAndFlush(aCase);

        /*
        По "поправка" на соодветниот проблем, доколку состојбата на случајот е CLOSED,
        тоа значи дека сите проблеми во рамки на случајот се разрешени.
        Во тој случај, се публикува настан (на кој е претплатен invoice-archive агрегатот).
        Целта на настанот е да извести за затворен случај, за кој претплатникот генерира фактура.
         */
        if (aCase.getState().equals(CaseState.CLOSE))
            domainEventPublisher
                    .publish(new VehicleCaseClosed(aCase.totalCost().getAmount(), aCase.getVehicleId().getId()));
    }

    private Case toDomainObject(CaseForm caseForm) {
        return new Case(caseForm.getTitle(), new VehicleId(caseForm.getVehicleId().getId()));
    }
}
