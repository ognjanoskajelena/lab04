package mk.ukim.finki.emt.casemanagement.domain.repository;

import mk.ukim.finki.emt.casemanagement.domain.models.Case;
import mk.ukim.finki.emt.casemanagement.domain.models.CaseId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaseRepository extends JpaRepository<Case, CaseId> {
}
