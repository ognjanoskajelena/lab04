package mk.ukim.finki.emt.casemanagement.service.forms;

import lombok.Data;
import mk.ukim.finki.emt.vehiclecatalogue.domain.models.VehicleId;

import javax.validation.constraints.NotNull;

@Data
public class CaseForm {

    private String title;

    @NotNull
    private VehicleId vehicleId;
}
