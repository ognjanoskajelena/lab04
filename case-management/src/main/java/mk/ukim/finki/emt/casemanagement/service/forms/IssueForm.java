package mk.ukim.finki.emt.casemanagement.service.forms;

import lombok.Data;
import mk.ukim.finki.emt.casemanagement.domain.models.CaseId;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Money;

import javax.validation.constraints.NotNull;

@Data
public class IssueForm {

    private String description;

    @NotNull
    private Money repairCost;

    @NotNull
    private CaseId caseId;
}
