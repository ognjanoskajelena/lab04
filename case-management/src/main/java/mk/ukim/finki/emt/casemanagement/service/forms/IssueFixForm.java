package mk.ukim.finki.emt.casemanagement.service.forms;

import lombok.Data;
import mk.ukim.finki.emt.casemanagement.domain.models.CaseId;
import mk.ukim.finki.emt.casemanagement.domain.models.IssueId;

import javax.validation.constraints.NotNull;

@Data
public class IssueFixForm {

    @NotNull
    private IssueId issueId;

    @NotNull
    private CaseId caseId;
}
