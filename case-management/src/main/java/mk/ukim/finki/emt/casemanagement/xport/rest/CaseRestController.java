package mk.ukim.finki.emt.casemanagement.xport.rest;

import lombok.AllArgsConstructor;
import mk.ukim.finki.emt.casemanagement.domain.models.Case;
import mk.ukim.finki.emt.casemanagement.domain.models.CaseId;
import mk.ukim.finki.emt.casemanagement.domain.models.Issue;
import mk.ukim.finki.emt.casemanagement.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.casemanagement.service.CaseService;
import mk.ukim.finki.emt.casemanagement.service.forms.CaseForm;
import mk.ukim.finki.emt.casemanagement.service.forms.IssueFixForm;
import mk.ukim.finki.emt.casemanagement.service.forms.IssueForm;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest controller за case-management агрегатот, кој комуницира со "надворешниот свет".
 */
@RestController
@RequestMapping("/api/case")
@CrossOrigin(origins = "http://localhost:3000")
@AllArgsConstructor
public class CaseRestController {

    private final CaseService caseService;

    @GetMapping("/{id}/all")
    public List<Case> findAllByVehicle(@PathVariable VehicleId id) {
        return this.caseService.findAllByVehicleId(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Case> findById(@PathVariable CaseId id) {
        return this.caseService.findById(id)
                .map(c -> ResponseEntity.ok().body(c))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @PostMapping("/add")
    public ResponseEntity<Case> save(@RequestBody CaseForm caseForm) {
        return this.caseService.openCase(caseForm)
                .map(c -> ResponseEntity.ok().body(c))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @GetMapping("/{id}/issues")
    public List<Issue> findAllIssuesByCase(@PathVariable CaseId id) {
        return this.caseService.findAllIssuesByCase(id);
    }

    @PostMapping("/add-issue")
    public ResponseEntity<Issue> addIssue(@RequestBody IssueForm issueForm) {
        return this.caseService.addIssue(issueForm)
                .map(c -> ResponseEntity.ok().body(c))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @PostMapping("/fix-issue")
    public void fixIssue(@RequestBody IssueFixForm fixForm) {
        this.caseService.fixIssue(fixForm);
    }
}
