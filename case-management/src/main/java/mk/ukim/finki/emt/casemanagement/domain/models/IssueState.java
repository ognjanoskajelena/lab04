package mk.ukim.finki.emt.casemanagement.domain.models;

public enum IssueState {
    NOT_FIXED, FIXED
}
