package mk.ukim.finki.emt.casemanagement.domain.models;

import mk.ukim.finki.emt.sharedkernel.domain.base.DomainObjectId;

public class IssueId extends DomainObjectId {

    private IssueId() {
        super(IssueId.randomId(IssueId.class).getId());
    }

    public IssueId(String uuid) {
        super(uuid);
    }
}
