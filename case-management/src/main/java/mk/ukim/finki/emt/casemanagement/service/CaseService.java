package mk.ukim.finki.emt.casemanagement.service;

import mk.ukim.finki.emt.casemanagement.domain.exceptions.*;
import mk.ukim.finki.emt.casemanagement.domain.models.Case;
import mk.ukim.finki.emt.casemanagement.domain.models.CaseId;
import mk.ukim.finki.emt.casemanagement.domain.models.Issue;
import mk.ukim.finki.emt.casemanagement.domain.models.IssueId;
import mk.ukim.finki.emt.casemanagement.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.casemanagement.service.forms.CaseForm;
import mk.ukim.finki.emt.casemanagement.service.forms.IssueFixForm;
import mk.ukim.finki.emt.casemanagement.service.forms.IssueForm;

import java.util.List;
import java.util.Optional;

public interface CaseService {

    Optional<Case> openCase(CaseForm caseForm);

    List<Case> findAllByVehicleId(VehicleId id);

    Optional<Case> findById(CaseId id);

    List<Issue> findAllIssuesByCase(CaseId id);

    Optional<Issue> addIssue(IssueForm issueForm) throws CaseNotFoundException;

    boolean findIssueByCase(CaseId caseId, IssueId issueId);

    void fixIssue(IssueFixForm fixForm) throws CaseNotFoundException, IssueNotFoundException;
}
