package mk.ukim.finki.emt.casemanagement.domain.models;

import lombok.Getter;
import mk.ukim.finki.emt.casemanagement.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.sharedkernel.domain.base.AbstractEntity;
import mk.ukim.finki.emt.sharedkernel.domain.base.DomainObjectId;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Money;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Класата Case e поставена да биде Aggregate Root на агрегатот case-management.
 * Во неа се чуваат Issues, кои се дел од конкретниот Case.
 * Преку овој ентитет се прават промените на истиот и на множеството од Issues.
 */
@Entity
@Table(name = "cases")
@Getter
public class Case extends AbstractEntity<CaseId> {

    private String title;

    @Enumerated(value = EnumType.STRING)
    private CaseState state;

    @AttributeOverride(name = "id", column = @Column(name = "vehicle_id", nullable = false))
    private VehicleId vehicleId;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<Issue> issues = new HashSet<>();

    private Case() {
        super(DomainObjectId.randomId(CaseId.class));
    }

    public Case(String title, VehicleId vehicleId) {
        super(DomainObjectId.randomId(CaseId.class));
        this.title = title;
        this.state = CaseState.OPEN;
        this.vehicleId = vehicleId;
    }

    public Money totalCost() {
        return issues.stream().map(Issue::getRepairCost)
                .reduce(new Money(0), Money::add);
    }

    public Issue addIssue(String description, @NonNull Money repairCost) {
        Objects.requireNonNull(repairCost, "repair cost must not be null");
        Issue issue = new Issue(description, repairCost);
        this.issues.add(issue);
        return issue;
    }

    public Case fixIssue(@NonNull IssueId issueId) {
        Objects.requireNonNull(issueId, "issue must not be null");
        issues.forEach(issue -> {
            if (issue.getId().getId().equals(issueId.getId())) {
                issue.fix();
            }
        });
        if (allIssuesFixed()) {
            /*
            Кога сите Issues кои се дел од конкретниот Case се разрешени (имаат state = FIXED),
            случајот добива state = CLOSE.
             */
            this.state = CaseState.CLOSE;
        }
        return this;
    }

    private boolean allIssuesFixed() {
        return issues.stream().allMatch(issue -> issue.getState().equals(IssueState.FIXED));
    }
}
