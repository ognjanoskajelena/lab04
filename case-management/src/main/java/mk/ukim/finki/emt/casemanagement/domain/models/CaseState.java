package mk.ukim.finki.emt.casemanagement.domain.models;

public enum CaseState {
    OPEN, CLOSE
}
