package mk.ukim.finki.emt.invoicearchive.service;

import mk.ukim.finki.emt.invoicearchive.domain.models.Invoice;
import mk.ukim.finki.emt.invoicearchive.domain.models.InvoiceId;
import mk.ukim.finki.emt.invoicearchive.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Money;

import java.util.List;
import java.util.Optional;

public interface InvoiceService {

    InvoiceId caseClosed(Money totalPrice, VehicleId vehicleId);

    List<Invoice> findAllByVehicleId(VehicleId id);

    Optional<Invoice> findById(InvoiceId id);
}
