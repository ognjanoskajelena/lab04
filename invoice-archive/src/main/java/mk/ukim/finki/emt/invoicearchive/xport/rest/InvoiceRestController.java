package mk.ukim.finki.emt.invoicearchive.xport.rest;

import lombok.AllArgsConstructor;
import mk.ukim.finki.emt.invoicearchive.domain.models.Invoice;
import mk.ukim.finki.emt.invoicearchive.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.invoicearchive.service.InvoiceService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest controller за invoice-archive агрегатот комуницира со "надворешниот свет".
 */
@RestController
@RequestMapping("/api/invoice")
@CrossOrigin(origins = "http://localhost:3000")
@AllArgsConstructor
public class InvoiceRestController {

    private final InvoiceService invoiceService;

    @GetMapping("/{id}/all")
    public List<Invoice> findAllByVehicle(@PathVariable VehicleId id) {
        return this.invoiceService.findAllByVehicleId(id);
    }
}
