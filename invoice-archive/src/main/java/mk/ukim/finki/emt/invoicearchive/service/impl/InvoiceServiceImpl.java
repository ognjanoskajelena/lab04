package mk.ukim.finki.emt.invoicearchive.service.impl;

import mk.ukim.finki.emt.invoicearchive.domain.models.Invoice;
import mk.ukim.finki.emt.invoicearchive.domain.models.InvoiceId;
import mk.ukim.finki.emt.invoicearchive.domain.repository.InvoiceRepository;
import mk.ukim.finki.emt.invoicearchive.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.invoicearchive.service.InvoiceService;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Money;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Сервисот за invoice-archive агрегатот ја оркестрира бизнис логиката инкорпорирана во доменот
 * и дополнително користи помошни технологии.
 */
@Service
public class InvoiceServiceImpl implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImpl(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public InvoiceId caseClosed(Money totalPrice, VehicleId vehicleId) {
        var invoice = invoiceRepository.saveAndFlush(Invoice.build(totalPrice, vehicleId));
        return invoice.getId();
    }

    @Override
    public List<Invoice> findAllByVehicleId(VehicleId id) {
        List<Invoice> result = new ArrayList<>();
        for (Invoice i : invoiceRepository.findAll()) {
            if (i.getVehicleId().getId().equals(id.getId())) {
                result.add(i);
            }
        }
        return result;
    }

    @Override
    public Optional<Invoice> findById(InvoiceId id) {
        return invoiceRepository.findById(id);
    }
}
