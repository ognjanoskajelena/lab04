package mk.ukim.finki.emt.invoicearchive.domain.valueobjects;

import mk.ukim.finki.emt.sharedkernel.domain.base.DomainObjectId;
import org.springframework.lang.NonNull;

/**
 * VehicleId претставува вредносен објект, настанат како резултат на формирање на invoice-archive
 * и vehicle-catalogue ограничните контексти.
 */
public class VehicleId extends DomainObjectId {

    private VehicleId() {
        super(VehicleId.randomId(VehicleId.class).getId());
    }

    public VehicleId(@NonNull String uuid) {
        super(uuid);
    }

    public static VehicleId of(String uuid) {
        return new VehicleId(uuid);
    }
}
