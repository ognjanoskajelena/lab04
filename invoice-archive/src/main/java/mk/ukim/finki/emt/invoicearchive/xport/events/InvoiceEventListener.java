package mk.ukim.finki.emt.invoicearchive.xport.events;

import lombok.AllArgsConstructor;
import mk.ukim.finki.emt.invoicearchive.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.invoicearchive.service.InvoiceService;
import mk.ukim.finki.emt.sharedkernel.domain.config.TopicHolder;
import mk.ukim.finki.emt.sharedkernel.domain.events.DomainEvent;
import mk.ukim.finki.emt.sharedkernel.domain.events.VehicleCaseClosed;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Money;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * Listener на настани, претплатен на topic - TOPIC_VEHICLE_CASE_CLOSED,
 * чија цел е примање на VehicleCaseClosed настанот, публикуван од агрегатот case-management,
 * и според податоците во истиот, креирање на фактура соодветна за затворениот случај.
 */
@Service
@AllArgsConstructor
public class InvoiceEventListener {

    private final InvoiceService invoiceService;

    @KafkaListener(topics = TopicHolder.TOPIC_VEHICLE_CASE_CLOSED, groupId = "vehicleCatalog")
    public void consumeOrderItemCreatedEvent(String jsonMessage) {
        try {
            VehicleCaseClosed event = DomainEvent.fromJson(jsonMessage, VehicleCaseClosed.class);
            invoiceService.caseClosed(Money.valueOf(event.getTotalCost()), VehicleId.of(event.getVehicleId()));
        } catch (Exception e) {

        }
    }
}
