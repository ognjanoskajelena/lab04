package mk.ukim.finki.emt.invoicearchive.domain.models;

import lombok.Getter;
import mk.ukim.finki.emt.invoicearchive.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.sharedkernel.domain.base.AbstractEntity;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Money;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.Instant;

/**
 * Ентитетот Invoice е единствениот во својот агрегат, па соодветно претставува Aggregate Root.
 */
@Entity
@Getter
@Table(name = "invoice")
public class Invoice extends AbstractEntity<InvoiceId> {

    private Money totalPrice;

    private Instant createdOn;

    private VehicleId vehicleId;

    private Invoice() {
        super(InvoiceId.randomId(InvoiceId.class));
    }

    public static Invoice build(Money totalPrice, VehicleId vehicleId) {
        Invoice invoice = new Invoice();
        invoice.totalPrice = totalPrice;
        invoice.createdOn = Instant.now();
        invoice.vehicleId = vehicleId;
        return invoice;
    }
}
