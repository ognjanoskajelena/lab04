package mk.ukim.finki.emt.invoicearchive.service.forms;

import lombok.Data;
import mk.ukim.finki.emt.invoicearchive.domain.valueobjects.VehicleId;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Money;

import javax.validation.constraints.NotNull;

@Data
public class InvoiceForm {

    @NotNull
    private Money totalPrice;

    @NotNull
    private VehicleId vehicleId;
}
