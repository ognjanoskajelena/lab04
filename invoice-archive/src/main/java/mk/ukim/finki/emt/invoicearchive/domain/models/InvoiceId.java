package mk.ukim.finki.emt.invoicearchive.domain.models;

import mk.ukim.finki.emt.sharedkernel.domain.base.DomainObjectId;
import org.springframework.lang.NonNull;

public class InvoiceId extends DomainObjectId {

    private InvoiceId() {
        super(InvoiceId.randomId(InvoiceId.class).getId());
    }

    public InvoiceId(@NonNull String uuid) {
        super(uuid);
    }

    public static InvoiceId of(String uuid) {
        return new InvoiceId(uuid);
    }
}
