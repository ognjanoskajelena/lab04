package mk.ukim.finki.emt.invoicearchive.domain.repository;

import mk.ukim.finki.emt.invoicearchive.domain.models.Invoice;
import mk.ukim.finki.emt.invoicearchive.domain.models.InvoiceId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceRepository extends JpaRepository<Invoice, InvoiceId> {
}
