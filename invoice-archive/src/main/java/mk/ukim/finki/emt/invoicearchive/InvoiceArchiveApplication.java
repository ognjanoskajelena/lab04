package mk.ukim.finki.emt.invoicearchive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoiceArchiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(InvoiceArchiveApplication.class, args);
    }

}
