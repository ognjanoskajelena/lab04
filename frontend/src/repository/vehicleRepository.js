import axios from "../custom-axios/axios-vehicle";

const VehicleService = {
  // methods that will handle http requests

    fetchVehicles: () => {
        return axios.get("/vehicle");
    },
    addVehicle: (brand, model, year, registration, power, fuelType) => {
        return axios.post("/vehicle/add", {
            "brand" : brand,
            "model" : model,
            "year" : year,
            "registration" : registration,
            "power" : power,
            "fuelType" : fuelType
        });
    },
    getVehicle: (id) => {
        return axios.get(`/vehicle/${id}`);
    }
};

export default VehicleService;