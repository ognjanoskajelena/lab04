import axios from "../custom-axios/axios-invoice";

const InvoiceService = {
    // methods that will handle http requests

    fetchInvoicesByVehicle: (id) => {
        return axios.get(`/invoice/${id}/all`);
    }
};

export default InvoiceService;