import axios from "../custom-axios/axios-case";

const CaseService = {
    // methods that will handle http requests

    fetchCasesByVehicle: (id) => {
        return axios.get(`/case/${id}/all`);
    },
    addCaseForVehicle: (title, vehicleId) => {
        return axios.post("/case/add", {
            "title" : title,
            "vehicleId" : vehicleId
        });
    },
    getCase: (id) => {
        return axios.get(`/case/${id}`);
    },
    fetchIssuesByCase: (id) => {
        return axios.get(`/case/${id}/issues`);
    },
    addIssueForCase: (description, repairCost, caseId) => {
        return axios.post(`/case/add-issue`, {
            "description" : description,
            "repairCost" : repairCost,
            "caseId" : caseId
        });
    },
    fixIssue: (id, caseId) => {
        return axios.post(`/case/fix-issue`, {
            "issueId" : id,
            "caseId" : caseId
        });
    }
};

export default CaseService;