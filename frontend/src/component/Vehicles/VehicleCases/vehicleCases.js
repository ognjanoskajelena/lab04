import React from "react";
import CaseTerm from "../../Case/CaseTerm/caseTerm"
import {Link} from "react-router-dom";

const VehicleCases = (props) => {
    return (
        <div className={"container mm-4 mt-5"}>
            <div className={"row"}>
                <div className={"col-8"}>
                    <table className={"table table-striped"}>
                        <thead>
                        <tr>
                            <th scope={"col"}>Title</th>
                            <th scope={"col"}>State</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.cases.map((term) => {
                            return (
                                <CaseTerm term={term} onAddIssue={props.onAddIssue} onAllIssues={props.onAllIssues}/>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="">
                <Link type="button" className="btn btn-light" to={"/vehicles"}>Back</Link>
            </div>
        </div>
    )
}

export default VehicleCases;