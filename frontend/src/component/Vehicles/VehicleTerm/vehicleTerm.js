import React from "react";
import {Link} from "react-router-dom";

const vehicleTerm = (props) => {
    return (
        <tr key={props.term.id.id}>
            <td scope={"col"}>{props.term.brand}</td>
            <td scope={"col"}>{props.term.model}</td>
            <td scope={"col"}>{props.term.year}</td>
            <td scope={"col"}>{props.term.registration}</td>
            <td scope={"col"}>{props.term.power.power} {props.term.power.unit}</td>
            <td scope={"col"}>{props.term.fuelType}</td>
            <td scope={"col"} className={"text-right"}>
                <Link title={"add case"}
                      className={"btn btn-primary m-2"}
                      onClick={() => props.onAddCase(props.term.id.id)}
                      to={`/vehicles/${props.term.id.id}/add-case`}>
                    Add case
                </Link>
                <Link title={"all cases"}
                      className={"btn btn-info text-light"}
                      onClick={() => props.onAllCases(props.term.id.id)}
                      to={`/vehicles/cases`}>
                    Cases
                </Link>
                <Link title={"all invoices"}
                      className={"btn btn-outline-dark m-2"}
                      onClick={() => props.onAllInvoices(props.term.id.id)}
                      to={`/vehicles/invoices`}>
                    Invoices
                </Link>
            </td>
        </tr>
    );
}

export default vehicleTerm;

