import React from "react";
import {useHistory} from "react-router-dom";
import {Link} from "react-router-dom";

const VehicleAdd = (props) => {

    const history = useHistory();
    const [formData, updateFormData] = React.useState({
        brand: "",
        model: "",
        year: "",
        registration: "",
        power: "",
        fuelType: "",
    });

    const handleChange = (e) => {
        // console.log(e)
        updateFormData({
            ...formData,
            [e.target.name]: e.target.value.trim()
        });
    }

    const onFormSubmit = (e) => {
        e.preventDefault();
        const brand = formData.brand;
        const model = formData.model;
        const year = formData.year;
        const registration = formData.registration;
        const power = formData.power;
        const fuelType = formData.fuelType;

        // console.log(e)
        props.onAddVehicle(brand, model, year, registration, power, fuelType);
        history.push("/vehicles");
    }

    return (
        <div className="container">
            <div className={"row mt-5 text-right"}>
                <div className={"col-12"}>
                    <h3>Register new vehicle</h3>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col md-5">
                    <form onSubmit={onFormSubmit}>
                        <div className="form-group">
                            <label htmlFor="brand">Brand</label>
                            <input type="text"
                                   id="brand"
                                   name="brand"
                                   className="form-control mb-3"
                                   placeholder="Enter vehicle brand"
                                   required
                                   onChange={handleChange}/>

                            <label htmlFor="model">Model</label>
                            <input type="text"
                                   id="model"
                                   name="model"
                                   className="form-control mb-3"
                                   placeholder="Enter vehicle model"
                                   required
                                   onChange={handleChange}/>

                            <label htmlFor="year">Year</label>
                            <input type="text"
                                   id="year"
                                   name="year"
                                   className="form-control mb-3"
                                   placeholder="Enter vehicle year"
                                   required
                                   onChange={handleChange}/>

                            <label htmlFor="registration">Plate number</label>
                            <input type="text"
                                   id="registration"
                                   name="registration"
                                   className="form-control mb-3"
                                   placeholder="Enter vehicle plate number"
                                   required
                                   onChange={handleChange}/>

                            <label htmlFor="power">Power (kW)</label>
                            <input type="number"
                                   id="power"
                                   name="power"
                                   className="form-control mb-3"
                                   placeholder="Enter vehicle power (in kW)"
                                   required
                                   onChange={handleChange}/>

                            <label htmlFor="fuelType">Fuel type</label>
                            <input type="text"
                                   id="fuelType"
                                   name="fuelType"
                                   className="form-control mb-3"
                                   placeholder="Enter vehicle fuel type"
                                   required
                                   onChange={handleChange}/>
                        </div>
                        <div className="mb-2">
                            <button type="submit" id="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </form>
                    <div className="">
                        <Link type="button" className="btn btn-light" to={"/vehicles"}>Back</Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default VehicleAdd;