import React from "react";
import VehicleTerm from "./VehicleTerm/vehicleTerm"
import {Link} from "react-router-dom";

const Vehicles = (props) => {
    return (
        <div className={"container mm-4 mt-5"}>
            <div className={"row my-5 pb-2"}>
                <h3>Car Workshop registered vehicles</h3>
            </div>
            <div className={"row"}>
                <div className={"table-responsive"}>
                    <table className={"table table-striped"}>
                        <thead>
                        <tr>
                            <th scope={"col"}>Brand</th>
                            <th scope={"col"}>Model</th>
                            <th scope={"col"}>Year</th>
                            <th scope={"col"}>Registration</th>
                            <th scope={"col"}>Power</th>
                            <th scope={"col"}>Fuel type</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.vehicles.map((term) => {
                            return (
                                <VehicleTerm term={term} onAddCase={props.onAddCase} onAllCases={props.onAllCases}
                                             onAllInvoices={props.onAllInvoices}/>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
                <div className="col mb-3">
                    <div className="row">
                        <div className="col-sm-12 col-md-12">
                            <Link className={"btn btn-dark mx-auto"} to={"/vehicles/add"}>Add new vehicle</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Vehicles;