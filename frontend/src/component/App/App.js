import './App.css';
import React, {Component} from "react";
import {BrowserRouter as Router, Redirect, Route} from "react-router-dom";

import Vehicles from "../Vehicles/vehicles";
import Header from "../Header/header";
import VehicleAdd from "../Vehicles/VehicleAdd/vehicleAdd"
import CaseAdd from "../Case/CaseAdd/caseAdd"
import VehicleCases from "../Vehicles/VehicleCases/vehicleCases"
import CaseIssues from "../Case/CaseIssues/caseIssues"
import IssueAdd from "../Issues/IssueAdd/issueAdd";
import VehicleInvoices from "../Invoice/vehicleInvoices";

import VehicleService from "../../repository/vehicleRepository";
import InvoiceService from "../../repository/invoiceRepository";
import CaseService from "../../repository/caseRepository";

/**
 * Root - компонента, одговорна за комуникацијата на останатите компоненти,
 * како дел од frontend интерфејсот на апликацијата.
 */
class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            vehicles: [],
            cases: [],
            invoices: [],
            selectedVehicle: {},
            selectedCase: {},
            issuesForCase: []
        }
    }

    render() {
        return (
            <Router>
                {/*nav*/}
                <Header/>

                {/*component-routes*/}
                <main>
                    <div className="container">

                        <Route path={"/vehicles/:id/add-case"} exact
                               render={() => <CaseAdd vehicle={this.state.selectedVehicle}
                                                      addCaseForVehicle={this.addCaseForVehicle}/>}/>

                        <Route path={"/vehicles/add"} exact
                               render={() => <VehicleAdd onAddVehicle={this.addVehicle}/>}/>

                        <Route path={"/vehicles/cases"} exact
                               render={() => <VehicleCases cases={this.state.cases} onAddIssue={this.getCase}
                                                           onAllIssues={this.getIssuesForCase}/>}/>

                        <Route path={"/vehicles/invoices"} exact
                               render={() => <VehicleInvoices invoices={this.state.invoices}/>}/>

                        <Route path={"/vehicles"} exact
                               render={() => <Vehicles vehicles={this.state.vehicles} onAddCase={this.getVehicle}
                                                       onAllCases={this.getCasesForVehicle}
                                                       onAllInvoices={this.getInvoicesForVehicle}/>}/>

                        <Route path={"/case/add-issue"} exact
                               render={() => <IssueAdd case={this.state.selectedCase}
                                                       addIssueForCase={this.addIssueForCase}/>}/>

                        <Route path={"/case/issues"} exact
                               render={() => <CaseIssues case={this.state.selectedCase}
                                                         issues={this.state.issuesForCase}
                                                         onFixIssue={this.fixIssue}/>}/>

                        <Redirect to={"/vehicles"}/>
                    </div>
                </main>
            </Router>
        );
    }

    loadVehicles = () => {
        VehicleService.fetchVehicles()
            .then((data) => {
                // console.log(data)
                this.setState({
                    vehicles: data.data
                });
            });
    }

    addVehicle = (brand, model, year, registration, power, fuelType) => {
        VehicleService.addVehicle(brand, model, year, registration, power, fuelType)
            .then(() => {
                this.loadVehicles();
            });
    }

    getVehicle = (id) => {
        VehicleService.getVehicle(id)
            .then((response) => {
                this.setState({
                    selectedVehicle: response.data
                })
            });
    }

    getCasesForVehicle = (id) => {
        CaseService.fetchCasesByVehicle(id)
            .then((data) => {
                this.setState({
                    cases: data.data
                })
            });
    }

    getCase = (id) => {
        CaseService.getCase(id)
            .then((data) => {
                this.setState({
                    selectedCase: data.data
                })
            });
    }

    getIssuesForCase = (id) => {
        CaseService.fetchIssuesByCase(id)
            .then((data) => {
                this.setState({
                    issuesForCase: data.data
                });
            });
        this.getCase(id);
    }

    getInvoicesForVehicle = (id) => {
        InvoiceService.fetchInvoicesByVehicle(id)
            .then((data) => {
                this.setState({
                    invoices: data.data
                });
            })
    }

    addCaseForVehicle = (title, vehicleId) => {
        CaseService.addCaseForVehicle(title, vehicleId)
            .then(() => {
                //
            });
    }

    fixIssue = (issueId, aCase) => {
        console.log(issueId, aCase.id)
        CaseService.fixIssue(issueId, aCase.id)
            .then(() => {
                //
            });
    }

    addIssueForCase = (description, repairCost, caseId) => {
        CaseService.addIssueForCase(description, repairCost, caseId)
            .then(() => {
                //
            });
    }

    componentDidMount() {
        this.loadVehicles();
    }
}

export default App;
