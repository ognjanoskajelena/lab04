import React from "react";
import InvoiceTerm from "../Invoice/InvoiceTerm/invoiceTerm"
import {Link} from "react-router-dom";

const VehicleInvoices = (props) => {
    return (
        <div className={"container mm-4 mt-5"}>
            <div className={"row"}>
                <div className={"col-8"}>
                    <table className={"table table-striped"}>
                        <thead>
                        <tr>
                            <th scope={"col"}>Created on</th>
                            <th scope={"col"}>Total price</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.invoices.map((term) => {
                            return (
                                <InvoiceTerm term={term} />
                            );
                        })}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="">
                <Link type="button" className="btn btn-light" to={"/vehicles"}>Back</Link>
            </div>
        </div>
    )
}

export default VehicleInvoices;