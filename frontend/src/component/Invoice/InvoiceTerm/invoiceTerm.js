import React from "react";

const invoiceTerm = (props) => {
    return (
        <tr key={props.term.id.id}>
            <td scope={"col"}>{props.term.createdOn}</td>
            <td scope={"col"}>{props.term.totalPrice.amount}</td>
        </tr>
    );
}

export default invoiceTerm;

