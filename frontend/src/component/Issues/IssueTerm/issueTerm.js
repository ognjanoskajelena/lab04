import React from "react";
import {Link} from "react-router-dom";

const issueTerm = (props) => {

    return (
        <tr key={props.term.id.id}>
            <td scope={"col"}>{props.term.description}</td>
            <td scope={"col"}>{props.term.state}</td>
            <td scope={"col"}>{props.term.repairCost.amount}</td>
            <td scope={"col"} className={"text-right"}>
                <Link title={"add issue"}
                      className={props.term.state === "FIXED" ? "btn btn-success ml-1 disabled" : "btn btn-success ml-1"}
                      onClick={() => props.onFixIssue(props.term.id, props.case)}
                      to={`/vehicles`}>
                    Fix issue
                </Link>
            </td>
        </tr>
    );
}

export default issueTerm;

