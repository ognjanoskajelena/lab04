import React from "react";
import {useHistory} from "react-router-dom";

const IssueAdd = (props) => {

    const history = useHistory();
    const [formData, updateFormData] = React.useState({
        description: "",
        repairCost: 0
    });

    const handleChange = (e) => {
        updateFormData({
            ...formData,
            [e.target.name]: e.target.value.trim()
        });
    }

    const onFormSubmit = (e) => {
        e.preventDefault();
        const description = formData.description;
        const repairCost = formData.repairCost;

        props.addIssueForCase(description, repairCost, props.case.id);
        history.push(`/vehicles`);
    }

    return (
        <div className="container">
            <div className={"row mt-5 text-right"}>
                <div className={"col-12"}>
                    <h3>Add new issue for "<b>{props.case.title}</b>"</h3>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col md-5">
                    <form onSubmit={onFormSubmit}>
                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <input type="text"
                                   id="description"
                                   name="description"
                                   className="form-control mb-3"
                                   placeholder="Enter issue description"
                                   required
                                   onChange={handleChange}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="repairCost">Repair cost</label>
                            <input type="number"
                                   id="repairCost"
                                   name="repairCost"
                                   className="form-control mb-3"
                                   placeholder="Enter issue repair cost"
                                   required
                                   onChange={handleChange}/>
                        </div>
                        <div className="mb-2">
                            <button type="submit" id="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default IssueAdd;