import React from "react";
import {useHistory} from "react-router-dom";
import {Link} from "react-router-dom";

const CaseAdd = (props) => {
    console.log(props.case)
    const history = useHistory();
    const [formData, updateFormData] = React.useState({
        title: ""
    });

    const handleChange = (e) => {
        updateFormData({
            ...formData,
            [e.target.name]: e.target.value.trim()
        });
    }

    const onFormSubmit = (e) => {
        e.preventDefault();
        const title = formData.title;
        const vehicleId = props.vehicle.id;

        props.addCaseForVehicle(title, vehicleId);
        history.push(`/vehicles`);
    }

    return (
        <div className="container">
            <div className={"row mt-5 text-right"}>
                <div className={"col-12"}>
                    <h3>Add new case for <b>{props.vehicle.registration}</b></h3>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col md-5">
                    <form onSubmit={onFormSubmit}>
                        <div className="form-group">
                            <label htmlFor="title">Title</label>
                            <input type="text"
                                   id="title"
                                   name="title"
                                   className="form-control mb-3"
                                   placeholder="Enter case title"
                                   required
                                   onChange={handleChange}/>
                        </div>
                        <div className="mb-2">
                            <button type="submit" id="submit" className="btn btn-primary">Submit</button>
                        </div>
                        <div className="">
                            <Link type="button" className="btn btn-light" to={`/vehicles`}>
                                Back
                            </Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CaseAdd;