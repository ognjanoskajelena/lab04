import React from "react";
import IssueTerm from "../../Issues/IssueTerm/issueTerm"

const caseIssues = (props) => {

    return (
        <div className={"container mm-4 mt-5"}>
            <div className={"row"}>
                <div className={"col-8"}>
                    <table className={"table table-striped"}>
                        <thead>
                        <tr>
                            <th scope={"col"}>Description</th>
                            <th scope={"col"}>Issue state</th>
                            <th scope={"col"}>Repair cost</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.issues.map((term) => {
                            return (
                                <IssueTerm term={term} case={props.case} onFixIssue={props.onFixIssue}/>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default caseIssues;