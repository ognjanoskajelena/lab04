import React from "react";
import {Link} from "react-router-dom";

const caseTerm = (props) => {
    return (
        <tr key={props.term.id.id}>
            <td scope={"col"}>{props.term.title}</td>
            <td scope={"col"}>{props.term.state}</td>

            <td scope={"col"} className={"text-right"}>
                <Link title={"add issue"}
                      className={props.term.state === "CLOSE" ? "btn btn-primary m-2 disabled" : "btn btn-primary m-2"}
                      onClick={() => props.onAddIssue(props.term.id.id)}
                      to={`/case/add-issue`}>
                    Add issue
                </Link>
                <Link title={"all issues"}
                      className={"btn btn-info text-light"}
                      onClick={() => props.onAllIssues(props.term.id.id)}
                      to={`/case/issues`}>
                    All issues
                </Link>
            </td>
        </tr>
    );
}

export default caseTerm;

