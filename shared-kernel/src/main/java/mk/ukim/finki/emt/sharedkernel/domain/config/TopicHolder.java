package mk.ukim.finki.emt.sharedkernel.domain.config;

public class TopicHolder {
    public final static String TOPIC_VEHICLE_CASE_CLOSED = "vehicle-case-closed";
}
