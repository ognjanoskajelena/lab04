package mk.ukim.finki.emt.sharedkernel.domain.events;

import lombok.Getter;
import mk.ukim.finki.emt.sharedkernel.domain.config.TopicHolder;

/**
 * Настан кој се публикува по затворање на конкретен случај и за кој се "слуша" со цел креирање на соодветна фактура.
 */
@Getter
public class VehicleCaseClosed extends DomainEvent {

    private int totalCost;

    private String vehicleId;

    public VehicleCaseClosed() {
        super(TopicHolder.TOPIC_VEHICLE_CASE_CLOSED);
    }

    public VehicleCaseClosed(int totalCost, String vehicleId) {
        super(TopicHolder.TOPIC_VEHICLE_CASE_CLOSED);
        this.totalCost = totalCost;
        this.vehicleId = vehicleId;
    }
}
