package mk.ukim.finki.emt.sharedkernel.domain.financial;

import lombok.Getter;
import mk.ukim.finki.emt.sharedkernel.domain.base.ValueObject;

import javax.persistence.Embeddable;
import java.util.Objects;

/**
 * Репрзентира објект кој содржи парична вредност и имплементира можни операции со истата.
 * Се претпоставува дека workshop - от што ја користи оваа апликација работи исклучително со македонски денар.
 */
@Embeddable
@Getter
public class Money implements ValueObject {

    private final int amount;

    protected Money() {
        this.amount = 0;
    }

    public Money(int amount) {
        this.amount = amount;
    }

    public static Money valueOf(int amount) {
        return new Money(amount);
    }

    public Money add(Money money) {
        return new Money(this.amount + money.amount);
    }

    public Money subtract(Money money) {
        return new Money(this.amount - money.amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return amount == money.amount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount);
    }
}
